package com.temanni.houssamtemanni_free2move_test.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.temanni.houssamtemanni_free2move_test.data.model.Car

@Dao
interface Dao {
    @get:Query("SELECT * FROM Car")
    val all: List<Car>

    @Insert
    fun insertAll(vararg items: Car)

    @Query("DELETE FROM Car")
    fun deleteAll()
}
