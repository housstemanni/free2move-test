package com.temanni.houssamtemanni_free2move_test.data.remote

import com.temanni.houssamtemanni_free2move_test.data.model.Car
import io.reactivex.Observable
import retrofit2.Response
import retrofit2.http.*

interface CarsApi {
    @GET("ncltg/6a74a0143a8202a5597ef3451bde0d5a/raw/8fa93591ad4c3415c9e666f888e549fb8f945eb7/tc-test-ios.json")
    fun getCars(): Observable<Response<List<Car>>>
}