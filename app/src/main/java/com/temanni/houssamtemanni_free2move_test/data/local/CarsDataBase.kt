package com.temanni.houssamtemanni_free2move_test.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.temanni.houssamtemanni_free2move_test.data.model.Car

@Database(entities = [Car::class], version = 1)
@TypeConverters(
    CarConverter::class,
    ListConverter::class
)
abstract class CarsDataBase : RoomDatabase() {
    abstract fun dao(): Dao

    companion object {
        private var instance: CarsDataBase? = null

        @Synchronized
        fun getInstance(context: Context): CarsDataBase {
            if (instance == null) {
                instance = Room.databaseBuilder(
                    context.applicationContext,
                    CarsDataBase::class.java,
                    "car_data_base"
                )
                    .fallbackToDestructiveMigration()
                    .build()
            }
            return instance as CarsDataBase
        }
    }
}
