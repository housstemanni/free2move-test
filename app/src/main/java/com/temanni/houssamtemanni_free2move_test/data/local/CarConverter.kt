package com.temanni.houssamtemanni_free2move_test.data.local

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.temanni.houssamtemanni_free2move_test.data.model.Car

class CarConverter {
    @TypeConverter
    fun fromString(value: String?): Car {
        return Gson().fromJson(value, Car::class.java)
    }

    @TypeConverter
    fun fromObject(car: Car): String {
        val gson = Gson()
        return gson.toJson(car)
    }
}