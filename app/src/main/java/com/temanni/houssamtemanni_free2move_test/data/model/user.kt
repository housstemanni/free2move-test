package com.temanni.houssamtemanni_free2move_test.data.model

data class User(val firstName: String, val lastName: String, val email: String, val address: String)