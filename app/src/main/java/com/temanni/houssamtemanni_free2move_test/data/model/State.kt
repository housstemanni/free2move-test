package com.temanni.houssamtemanni_free2move_test.data.model

import androidx.lifecycle.MutableLiveData

sealed class State

object StateInitial : State()
object StateLoadSuccess : State()
object StateProgress : State()
data class StateError(val throwable: Throwable) : State()

class StateLiveData(state: State = StateInitial) : MutableLiveData<State>() {
    init {
        value = state
    }
}