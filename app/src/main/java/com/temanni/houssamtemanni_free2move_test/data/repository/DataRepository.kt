package com.temanni.houssamtemanni_free2move_test.data.repository

import android.annotation.SuppressLint
import android.content.Context
import android.net.NetworkInfo
import androidx.annotation.VisibleForTesting
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import com.temanni.houssamtemanni_free2move_test.utils.ConnectivityHelper
import com.temanni.houssamtemanni_free2move_test.data.local.CarsDataBase
import com.temanni.houssamtemanni_free2move_test.data.model.Car
import com.temanni.houssamtemanni_free2move_test.data.remote.CarsApi
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.lang.RuntimeException
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DataRepository @Inject constructor() {
    @Inject
    lateinit var context: Context

    @Inject
    lateinit var carsDataBase: CarsDataBase

    @Inject
    lateinit var connectivityHelper: ConnectivityHelper

    @Inject
    lateinit var api: CarsApi

    @SuppressLint("CheckResult")
    fun getCars(): Observable<List<Car>> {
        return connectivityHelper.isConnected()
            .subscribeOn(Schedulers.io())
            .concatMap { isConnected ->
                if (!isConnected) {
                    carsDataBase.dao().all.let {
                        if (it.isNotEmpty()) {
                            //If db is not empty then show local data
                            Observable.fromCallable { carsDataBase.dao().all }
                        } else {
                            // If db is empty then throw error and show snackBar with retry button
                            throw RuntimeException("No data to load")
                        }
                    }
                } else {
                    // If internet if available :
                    // 1 - We make a network call
                    // 2 - Update data base if success
                    // 3- return data from db
                    api.getCars().map { t ->
                        carsDataBase.dao().deleteAll()
                        t.body()?.let { cars ->
                            carsDataBase.dao().insertAll(*cars.toTypedArray())
                        }
                        carsDataBase.dao().all
                    }
                }
            }
    }

    @VisibleForTesting(otherwise = VisibleForTesting.NONE)
    fun initValues(
        context: Context,
        dataBase: CarsDataBase,
        connectivityHelper: ConnectivityHelper,
        carsApi: CarsApi
    ) {
        this.context = context
        this.carsDataBase = dataBase
        this.connectivityHelper = connectivityHelper
        this.api = carsApi
    }
}


