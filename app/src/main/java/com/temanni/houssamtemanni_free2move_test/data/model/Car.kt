package com.temanni.houssamtemanni_free2move_test.data.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class Car(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    @SerializedName("make") val makeName: String,
    val model: String,
    val year: Int,
    @SerializedName("picture") val pictureUrl: String,
    val equipments: List<String>?
) : Parcelable



