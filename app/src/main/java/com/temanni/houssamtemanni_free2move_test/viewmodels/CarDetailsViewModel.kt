package com.temanni.houssamtemanni_free2move_test.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.temanni.houssamtemanni_free2move_test.data.model.Car

class CarDetailsViewModel(car: Car?) : ViewModel() {
    private val _pictureUrl = MutableLiveData<String>()
    val pictureUrl = _pictureUrl

    private val _makeName = MutableLiveData<String>()
    val makeName = _makeName
    private val _model = MutableLiveData<String>()
    val model = _model

    private val _year = MutableLiveData<String>()
    val year = _year

    private val _equipments = MutableLiveData<List<String>>()
    val equipments = _equipments

    init {
        _pictureUrl.value = car?.pictureUrl
        _makeName.value = car?.makeName
        _model.value = car?.model
        _year.value = car?.year.toString()
        _equipments.value = car?.equipments
    }
}