package com.temanni.houssamtemanni_free2move_test.viewmodels

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.temanni.houssamtemanni_free2move_test.data.model.Car

class CarViewModel : ViewModel() {

    private lateinit var onItemClick: (Car) -> Unit
    private lateinit var car: Car

    private val _pictureUrl = MutableLiveData<String>()
    val pictureUrl = _pictureUrl

    private val _makeName = MutableLiveData<String>()
    val makeName = _makeName

    private val _year = MutableLiveData<String>()
    val year = _year

    private val _model = MutableLiveData<String>()
    val model = _model

    private val _searchString = MutableLiveData<String>()
    val searchString = _searchString


    fun bind(
        car: Car,
        onItemClick: (Car) -> Unit,
        searchString: String
    ) {
        this.car = car
        _pictureUrl.value = car.pictureUrl
        _makeName.value = car.makeName
        _model.value = car.model
        _year.value = car.year.toString()
        _searchString.value = searchString
        this.onItemClick = onItemClick
    }

    fun onItemClick(view: View) {
        onItemClick(car)
    }

}