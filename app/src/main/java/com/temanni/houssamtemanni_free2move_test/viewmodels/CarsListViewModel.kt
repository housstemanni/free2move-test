package com.temanni.houssamtemanni_free2move_test.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.temanni.houssamtemanni_free2move_test.data.model.StateError
import com.temanni.houssamtemanni_free2move_test.data.model.StateLiveData
import com.temanni.houssamtemanni_free2move_test.data.model.StateLoadSuccess
import com.temanni.houssamtemanni_free2move_test.utils.ConnectivityHelper
import com.temanni.houssamtemanni_free2move_test.data.model.Car
import com.temanni.houssamtemanni_free2move_test.data.repository.DataRepository
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import javax.inject.Inject

class CarsListViewModel : ViewModel() {

    @Inject
    lateinit var repository: DataRepository

    @Inject
    lateinit var connectivityHelper: ConnectivityHelper

    private val _state = StateLiveData()
    val state = _state

    private val _cars = MutableLiveData<List<Car>>()
    val cars = _cars

    private val disposables = CompositeDisposable()

    fun loadCars() {
        disposables.add(
            repository.getCars()
                .observeOn(AndroidSchedulers.mainThread()).subscribe({ cars ->
                    _cars.postValue(cars)
                    _state.postValue(StateLoadSuccess)
                }, {
                    state.postValue(
                        StateError(
                            it
                        )
                    )
                })
        )
    }

    override fun onCleared() {
        super.onCleared()
        disposables.clear()

    }
}