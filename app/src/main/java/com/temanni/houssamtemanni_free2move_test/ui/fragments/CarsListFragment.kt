package com.temanni.houssamtemanni_free2move_test.ui.fragments

import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.appcompat.widget.SearchView
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.snackbar.Snackbar
import com.jakewharton.rxbinding3.appcompat.queryTextChanges
import com.temanni.houssamtemanni_free2move_test.data.model.State
import com.temanni.houssamtemanni_free2move_test.data.model.StateError
import com.temanni.houssamtemanni_free2move_test.data.model.StateProgress
import com.temanni.houssamtemanni_free2move_test.utils.appComponent
import com.temanni.houssamtemanni_free2move_test.utils.hide
import com.temanni.houssamtemanni_free2move_test.utils.show
import com.temanni.houssamtemanni_free2move_test.R
import com.temanni.houssamtemanni_free2move_test.data.model.Car
import com.temanni.houssamtemanni_free2move_test.databinding.FragmentListCarsBinding
import com.temanni.houssamtemanni_free2move_test.ui.activities.CarDetailsActivity
import com.temanni.houssamtemanni_free2move_test.ui.adapters.CarsListAdapter
import com.temanni.houssamtemanni_free2move_test.viewmodels.CarsListViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import kotlinx.android.synthetic.main.fragment_list_cars.*
import java.util.concurrent.TimeUnit

class CarsListFragment : Fragment() {
    private lateinit var binding: FragmentListCarsBinding
    private lateinit var viewModel: CarsListViewModel
    private lateinit var adapter: CarsListAdapter
    private var snackbar: Snackbar? = null
    private val disposable = CompositeDisposable()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_list_cars,
            container,
            false
        );
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setAdapter()
        viewModel = ViewModelProviders.of(this)
            .get(CarsListViewModel::class.java)
            .apply {
                activity?.appComponent?.inject(this)
                state.observe(viewLifecycleOwner, Observer {
                    onStateChanged(it)
                })
                cars.observe(viewLifecycleOwner, Observer {
                    adapter.submitItems(it)
                })
                loadCars()
            }
        setHasOptionsMenu(true)
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.menu, menu)
        val searchItem = menu.findItem(R.id.searchBar)
        val searchView = searchItem.actionView as SearchView
        setSearchViewTextChangeListener(searchView)
    }

    private fun setSearchViewTextChangeListener(searchView: SearchView) {
        disposable.add(searchView.queryTextChanges().skipInitialValue()
            .debounce(300, TimeUnit.MILLISECONDS)
            .subscribeOn(AndroidSchedulers.mainThread())
            .observeOn(AndroidSchedulers.mainThread())
            .doOnError {
                Log.e("-HT-", "Error searching cars")
            }
            .subscribe {
                adapter.filter.filter(it)
            })
    }

    override fun onStop() {
        super.onStop()
        disposable.dispose()
    }

    private fun setAdapter() {
        val layoutManager = LinearLayoutManager(context)
        layoutManager.isSmoothScrollbarEnabled = true
        binding.apply {
            recyclerView.layoutManager = layoutManager
            adapter =
                CarsListAdapter {
                    goToDetails(it)
                }
            recyclerView.adapter = adapter
        }
    }

    private fun onStateChanged(state: State?) {
        updateProgress(state is StateProgress)
        if (state is StateError) showError()
    }

    private fun updateProgress(isProcess: Boolean) {
        if (isProcess) progress.show() else progress.hide()
    }

    private fun showError() {
        snackbar = Snackbar.make(root, R.string.error_message, Snackbar.LENGTH_INDEFINITE)
            .setAction(R.string.retry) {
                viewModel.loadCars()
            }
        snackbar?.show()
    }

    private fun goToDetails(car: Car) {
        startActivity(CarDetailsActivity.intent(requireContext(), car))
    }
}