package com.temanni.houssamtemanni_free2move_test.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.temanni.houssamtemanni_free2move_test.R
import com.temanni.houssamtemanni_free2move_test.data.model.Car
import com.temanni.houssamtemanni_free2move_test.databinding.CarItemBinding
import com.temanni.houssamtemanni_free2move_test.viewmodels.CarViewModel

class CarsListAdapter(
    private val onItemClick: (Car) -> Unit
) : RecyclerView.Adapter<CarsListAdapter.ViewHolder>(), Filterable {

    private val itemsList: MutableList<Car> = mutableListOf()
    private val filteredList: MutableList<Car> = mutableListOf()
    var searchString = ""

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: CarItemBinding = DataBindingUtil.inflate(
            LayoutInflater.from(parent.context),
            R.layout.car_item,
            parent,
            false
        )
        return ViewHolder(
            binding
        )
    }


    override fun getItemCount(): Int {
        return filteredList.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = filteredList[position]
        holder.bind(item, onItemClick, searchString)
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun publishResults(
                charSequence: CharSequence?,
                filterResults: FilterResults
            ) {
                applyFiler(filterResults.values as List<Car>)
            }

            override fun performFiltering(charSequence: CharSequence?): FilterResults {
                val queryString = charSequence?.toString()?.toLowerCase()
                if (queryString != null) {
                    searchString = queryString
                }

                val filterResults = FilterResults()

                filterResults.values = if (queryString == null || queryString.isEmpty())
                    itemsList
                else
                    itemsList.filter {
                        it.makeName.toLowerCase().contains(queryString.toLowerCase()) ||
                                it.model.toLowerCase().contains(queryString)

                    }
                return filterResults
            }
        }
    }

    fun submitItems(cars: List<Car>) {
        itemsList.clear()
        itemsList.addAll(cars)
        filteredList.clear()
        filteredList.addAll(cars)
        notifyDataSetChanged()
    }

    fun applyFiler(cars: List<Car>) {
        filteredList.clear()
        filteredList.addAll(cars)
        notifyDataSetChanged()
    }


    inner class ViewHolder(private val binding: CarItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            item: Car,
            onItemClick: (Car) -> Unit,
            searchString: String
        ) {
            val viewModel =
                CarViewModel()
            viewModel.bind(item, onItemClick, searchString)
            binding.viewModel = viewModel
        }
    }
}
