package com.temanni.houssamtemanni_free2move_test.ui.fragments

import android.app.Activity.RESULT_OK
import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Base64
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.google.gson.Gson
import com.temanni.houssamtemanni_free2move_test.utils.PREFERENCE_IMAGE_KEY
import com.temanni.houssamtemanni_free2move_test.utils.PREFERENCE_USER_KEY
import com.temanni.houssamtemanni_free2move_test.R
import com.temanni.houssamtemanni_free2move_test.data.model.User
import com.temanni.houssamtemanni_free2move_test.databinding.FragmentUserProfileBinding
import com.temanni.houssamtemanni_free2move_test.utils.appComponent
import java.io.ByteArrayOutputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStream
import javax.inject.Inject

const val PICK_IMAGE_REQUEST = 1212

class UserProfileFragment : Fragment() {

    @Inject
    lateinit var sharedPreferences: SharedPreferences
    private lateinit var binding: FragmentUserProfileBinding

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.fragment_user_profile,
            container,
            false
        );
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        activity?.appComponent?.inject(this)
        initListeners()
        loadLocalDataIfNeeded()
    }

    private fun initListeners() {
        binding.saveButton.setOnClickListener {
            binding.apply {
                val user =
                    User(
                        firstName.text.toString(),
                        lastName.text.toString(),
                        email.text.toString(),
                        address.text.toString()
                    )
                sharedPreferences?.edit()?.putString(PREFERENCE_USER_KEY, Gson().toJson(user))
                    ?.apply()
                Toast.makeText(
                    requireContext(),
                    requireContext().getString(R.string.profile_update_toast),
                    Toast.LENGTH_SHORT
                ).show()
            }

        }
        binding.updatePicture.setOnClickListener {
            selectImage()
        }
    }

    private fun loadLocalDataIfNeeded() {
        sharedPreferences?.getString(PREFERENCE_IMAGE_KEY, null)?.let {
            binding.imageView.setImageBitmap(decodeToBase64(it))
        }

        var user = Gson().fromJson(
            sharedPreferences?.getString(
                PREFERENCE_USER_KEY,
                null
            ), User::class.java
        )
        user?.let {
            binding.apply {
                firstName.setText(it.firstName)
                lastName.setText(it.lastName)
                email.setText(it.email)
                address.setText(it.address)
            }
        }
    }

    private fun selectImage() {
        val intent = Intent()
        intent.type = "image/*"
        intent.action = Intent.ACTION_GET_CONTENT
        intent.addCategory(Intent.CATEGORY_OPENABLE)
        startActivityForResult(intent, PICK_IMAGE_REQUEST)
    }

    override fun onActivityResult(
        requestCode: Int,
        resultCode: Int,
        data: Intent?
    ) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == PICK_IMAGE_REQUEST && resultCode == RESULT_OK && data != null && data.data != null) {
            val stream: InputStream
            try {
                Toast.makeText(activity, "Image saved", Toast.LENGTH_SHORT).show()
                stream = requireContext().contentResolver.openInputStream(data.data!!)!!
                val bitmap = BitmapFactory.decodeStream(stream)
                binding.imageView.setImageBitmap(bitmap)
                val editor = sharedPreferences.edit()
                editor.putString(PREFERENCE_IMAGE_KEY, encodeToBase64(bitmap))
                editor.commit()
            } catch (e: FileNotFoundException) {
                e.printStackTrace()
            } catch (e: IOException) {

                e.printStackTrace()
            }
        }
    }

    private fun encodeToBase64(image: Bitmap): String? {
        val outPutStream = ByteArrayOutputStream()
        image.compress(Bitmap.CompressFormat.PNG, 100, outPutStream)
        val b: ByteArray = outPutStream.toByteArray()
        return Base64.encodeToString(b, Base64.DEFAULT)
    }

    private fun decodeToBase64(input: String?): Bitmap? {
        val decodedByte: ByteArray = Base64.decode(input, 0)
        return BitmapFactory.decodeByteArray(decodedByte, 0, decodedByte.size)
    }
}

