package com.temanni.houssamtemanni_free2move_test.ui.activities

import ViewPagerFragmentAdapter
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.tabs.TabLayoutMediator
import com.temanni.houssamtemanni_free2move_test.R
import com.temanni.houssamtemanni_free2move_test.ui.fragments.CarsListFragment
import com.temanni.houssamtemanni_free2move_test.ui.fragments.UserProfileFragment
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    private var viewPager: ViewPager2? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewPager = findViewById(R.id.viewpager)
        viewPager?.orientation = ViewPager2.ORIENTATION_HORIZONTAL
        viewPager?.adapter = ViewPagerFragmentAdapter(
            supportFragmentManager,
            lifecycle,
            listOf(CarsListFragment(), UserProfileFragment())
        )
        TabLayoutMediator(tabs, viewpager) { tab, position ->
            tab.text = when (position) {
                0 -> getText(R.string.cars_tab)
                else -> getText(R.string.profil_tab)
            }
        }.attach()
    }
}