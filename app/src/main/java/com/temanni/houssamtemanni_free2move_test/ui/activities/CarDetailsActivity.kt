package com.temanni.houssamtemanni_free2move_test.ui.activities

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.databinding.DataBindingUtil
import com.temanni.houssamtemanni_free2move_test.R
import com.temanni.houssamtemanni_free2move_test.data.model.Car
import com.temanni.houssamtemanni_free2move_test.databinding.ActivityCarDetailsBinding
import com.temanni.houssamtemanni_free2move_test.viewmodels.CarDetailsViewModel

const val CAR_EXTRA = "CAR_EXTRA"

class CarDetailsActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCarDetailsBinding
    private lateinit var viewModel: CarDetailsViewModel
    private  var car: Car? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_car_details)
        car = intent.getParcelableExtra(CAR_EXTRA)
        viewModel = CarDetailsViewModel(car)
        binding.viewModel = viewModel
    }

    companion object {
        fun intent(context: Context, car: Car): Intent {
            val intent = Intent(context, CarDetailsActivity::class.java)
            intent.putExtra(CAR_EXTRA, car)
            return intent
        }
    }
}
