package com.temanni.houssamtemanni_free2move_test.utils

import android.view.View
import androidx.fragment.app.FragmentActivity
import com.temanni.houssamtemanni_free2move_test.app.CarsApplication
import com.temanni.houssamtemanni_free2move_test.di.component.AppComponent

val FragmentActivity.appComponent: AppComponent get() = (application as CarsApplication).appComponent
fun View.show() = let { visibility = View.VISIBLE }
fun View.hide() = let { visibility = View.GONE }

