package com.temanni.houssamtemanni_free2move_test.utils

const val BASE_URL = "https://gist.githubusercontent.com/"

const val CONNECTION_TIMEOUT = 30
const val READ_TIMEOUT = 30
const val SHARED_PREFERENCE_KEY = "SHARED_PREFERENCE_KEY"
const val PREFERENCE_USER_KEY = "SHARED_PREFERENCE_USER_KEY"
const val PREFERENCE_IMAGE_KEY = "PREFERENCE_IMAGE_KEY"

