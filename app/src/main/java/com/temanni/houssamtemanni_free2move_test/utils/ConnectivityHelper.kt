package com.temanni.houssamtemanni_free2move_test.utils

import android.content.Context
import android.net.NetworkInfo
import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork
import io.reactivex.Observable

class ConnectivityHelper(val context: Context) {
    fun isConnected(): Observable<Boolean> {
        return ReactiveNetwork
            .observeNetworkConnectivity(context.applicationContext)
            .map { connectivity -> connectivity.state() == NetworkInfo.State.CONNECTED }
    }
}