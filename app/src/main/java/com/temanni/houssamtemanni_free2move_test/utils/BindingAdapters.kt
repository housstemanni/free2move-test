package com.temanni.houssamtemanni_free2move_test.utils

import android.content.res.ColorStateList
import android.graphics.Color
import android.graphics.Typeface
import android.text.Spannable
import android.text.SpannableString
import android.text.style.TextAppearanceSpan
import android.widget.ImageView
import android.widget.TextView
import androidx.databinding.BindingAdapter
import com.bumptech.glide.GenericTransitionOptions
import com.bumptech.glide.Glide
import com.temanni.houssamtemanni_free2move_test.R

@BindingAdapter("bind:imageUrl")
fun loadImage(view: ImageView, imageUrl: String?) {
    Glide.with(view.context)
        .load(imageUrl)
        .placeholder(
            R.mipmap.ic_car
        )
        .into(view)
}

@BindingAdapter("bind:formattedEquipments")
fun setFormattedEquipments(view: TextView, equipments: List<String>?) {
    view.text = if ((equipments != null) && (equipments.isNotEmpty())) {
        equipments.joinToString(separator = ",\n")
    } else {
        view.context.getText(R.string.no_equipments)
    }
}

@BindingAdapter("bind:keyWord", "bind:text")
fun highlightTextIfNeeded(
    textView: TextView,
    keyWord: String,
    text: String
) {
    val fullText: String = text
    val mSearchText: String = keyWord
    if (mSearchText != null && mSearchText.isNotEmpty()) {
        val startPos: Int =
            fullText.toLowerCase().indexOf(mSearchText.toLowerCase())
        val endPos: Int = startPos + mSearchText.length
        if (startPos != -1) {
            val spannable: Spannable = SpannableString(fullText)
            val highlightColor =
                ColorStateList(arrayOf(intArrayOf()), intArrayOf(Color.YELLOW))
            val highlightSpan =
                TextAppearanceSpan(null, Typeface.BOLD, -1, highlightColor, null)
            spannable.setSpan(
                highlightSpan,
                startPos,
                endPos,
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE
            )
            textView.text = spannable
        } else {
            textView.text = fullText
        }
    } else {
        textView.text = fullText
    }
}


