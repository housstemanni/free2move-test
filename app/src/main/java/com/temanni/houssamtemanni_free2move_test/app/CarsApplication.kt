package com.temanni.houssamtemanni_free2move_test.app

import android.app.Application
import com.temanni.houssamtemanni_free2move_test.di.component.AppComponent
import com.temanni.houssamtemanni_free2move_test.di.component.DaggerAppComponent
import com.temanni.houssamtemanni_free2move_test.di.module.ApiModule
import com.temanni.houssamtemanni_free2move_test.di.module.AppModule


class CarsApplication : Application() {
    @Suppress("DEPRECATION")
    val appComponent: AppComponent by lazy {
        DaggerAppComponent.builder()
            .appModule(
                AppModule(
                    applicationContext
                )
            )
            .apiModule(ApiModule())
            .build()
    }

}