package com.temanni.houssamtemanni_free2move_test.di.component

import com.temanni.houssamtemanni_free2move_test.di.module.ApiModule
import com.temanni.houssamtemanni_free2move_test.di.module.AppModule
import com.temanni.houssamtemanni_free2move_test.ui.fragments.UserProfileFragment
import com.temanni.houssamtemanni_free2move_test.viewmodels.CarsListViewModel
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class, ApiModule::class])
interface AppComponent {
    fun inject(injected: UserProfileFragment)
    fun inject(injected: CarsListViewModel)
}