package com.temanni.houssamtemanni_free2move_test.di.module

import android.content.Context
import android.content.SharedPreferences
import com.temanni.houssamtemanni_free2move_test.data.local.CarsDataBase
import com.temanni.houssamtemanni_free2move_test.utils.ConnectivityHelper
import com.temanni.houssamtemanni_free2move_test.utils.SHARED_PREFERENCE_KEY
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/*
 * Module provides dependencies using application context
 */
@Module
class AppModule(private val context: Context) {

    @Singleton
    @Provides
    fun providesDataBase(): CarsDataBase = CarsDataBase.getInstance(context)

    @Provides
    fun providesContext(): Context = context

    @Provides
    fun providesConnectivityService(): ConnectivityHelper =
        ConnectivityHelper(context)

    @Singleton
    @Provides
    fun providesSharedPreferences(): SharedPreferences = context.getSharedPreferences(
        SHARED_PREFERENCE_KEY, Context.MODE_PRIVATE
    )
}