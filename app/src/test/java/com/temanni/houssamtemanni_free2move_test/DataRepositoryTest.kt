package com.temanni.houssamtemanni_free2move_test

import android.content.Context
import com.temanni.houssamtemanni_free2move_test.data.local.CarsDataBase
import com.temanni.houssamtemanni_free2move_test.data.model.Car
import com.temanni.houssamtemanni_free2move_test.data.remote.CarsApi
import com.temanni.houssamtemanni_free2move_test.data.repository.DataRepository
import com.temanni.houssamtemanni_free2move_test.utils.ConnectivityHelper
import io.mockk.*
import io.mockk.impl.annotations.MockK
import io.mockk.impl.annotations.RelaxedMockK
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.plugins.RxJavaPlugins
import org.junit.Test

import org.junit.Before
import retrofit2.Response


class DataRepositoryTest {

    @RelaxedMockK
    private lateinit var carsDataBase: CarsDataBase

    @MockK
    private lateinit var connectivityHelper: ConnectivityHelper

    @RelaxedMockK
    private lateinit var itemTestObserver: Observer<List<Car>>

    @MockK
    private lateinit var context: Context

    @MockK
    lateinit var api: CarsApi

    private lateinit var dataRepository: DataRepository

    private var fakeCarList = listOf(
        Car(
            id = 1,
            makeName = "peugeot",
            model = "206",
            year = 2001,
            equipments = listOf("eq1", "equ2"),
            pictureUrl = "httpss://url"
        ),
        Car(
            id = 2,
            makeName = "renault",
            model = "clio",
            year = 1990,
            equipments = listOf("eq3", "equ4"),
            pictureUrl = "httpss://url"
        )
    )


    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        RxJavaPlugins.setErrorHandler { t -> println("error = " + t.message) }
        dataRepository = DataRepository()
        dataRepository.initValues(
            dataBase = carsDataBase,
            connectivityHelper = connectivityHelper,
            context = context,
            carsApi = api
        )
    }

    @Test
    fun `should invoke error when offline and data base is empty`() {
        every { connectivityHelper.isConnected() } returns Observable.just(false)
        every { carsDataBase.dao().all } returns emptyList()
        dataRepository.getCars().subscribe(itemTestObserver)
        verify { itemTestObserver.onError(any()) }
    }


    @Test
    fun `should save fetched data in db when network is available`() {
        every { connectivityHelper.isConnected() } returns Observable.just(true)
        every { api.getCars() } returns Observable.just(Response.success(200, fakeCarList))
        every { carsDataBase.dao().deleteAll() } just Runs
        dataRepository.getCars().subscribe(itemTestObserver)
        verify { carsDataBase.dao().insertAll(*fakeCarList.toTypedArray()) }
        verify { itemTestObserver.onComplete() }
    }

    @Test
    fun `should return local data when offline and data base is not empty`() {
        val slot = slot<List<Car>>()
        every { connectivityHelper.isConnected() } returns Observable.just(false)
        every { carsDataBase.dao().all } returns fakeCarList
        dataRepository.getCars().subscribe(itemTestObserver)
        verify { itemTestObserver.onNext(capture(slot)) }
        assert(slot.captured == fakeCarList)
    }
}