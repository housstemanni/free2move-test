# FREE2MOVE technical test

## This repository contains an Android application that allows user to :
- Fetch cars list from api 
- Make search in fetched list 
- Acces to local data when network is not available
- Edit profile info and picture and persist changes locally  

## Used language and libraries
- Kotlin - primary project language
- Android Architecture Components - the core of MVVM pattern
- RxJava - efficient way to manage data chains
- Dagger - dependency injection framework
- Retrofit - to perform API call
- Glide - fluid way to load and cache images
- Room - for data persistence used in offline mode
- Reactivenetwork - allows listening to network connection state and Internet connectivity with Reactive Programming approach

## Architecture (inspired from one of my other projects in this [repository](https://bitbucket.org/housstemanni/git-hub-api/src/master/) )
The application is based on MVVM architecture and The project contains the following packages :

- data: contains all the data accessing and manipulating components.
- di: Dependency injection classes, mainly modules and components.
- ui: view classes and their adapters
- viewmodels: view models corresponding to each view 
- utils: Utility classes and helpers
- app : Custom Application class implementation to be adapted to injection 

## Testing
- Implemented unit test for DataRepository to test different scenarios of fetching data depending on network availability

## Things that could be improved:
- Improve UI/UX for edit profil screen (eg: add input control...), and implement unit tests for it.